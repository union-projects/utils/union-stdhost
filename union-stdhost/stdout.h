#pragma once

#include <Windows.h>
#include <stdio.h>

class Stdout {
  HANDLE ConsoleHandle;
  FILE* OutputFile;
  
  Stdout() {
    ConsoleHandle = GetStdHandle( STD_OUTPUT_HANDLE );
  }

public:
  void Open( bool append ) {
    DuplicateHandle( GetCurrentProcess(),
      ConsoleHandle,
      GetCurrentProcess(),
      &ConsoleHandle,
      0,
      FALSE,
      DUPLICATE_SAME_ACCESS
    );

    freopen_s( &OutputFile, GetOutputFileName(), append ? "a+" : "w+", stdout);
  }

  HANDLE GetConsoleHandle() {
    return ConsoleHandle;
  }

  void WriteToConsoleA( const char* data, DWORD length = -1 ) {
    if( length == -1 )
      length = lstrlenA( data );

    DWORD written;
    WriteConsoleA( ConsoleHandle, data, length, &written, 0 );
  }

  void WriteToConsoleW( const wchar_t* data, DWORD length = -1 ) {
    if( length == -1 )
      length = lstrlenW( data );

    DWORD written;
    WriteConsoleW( ConsoleHandle, data, length, &written, 0 );
  }

  static const char* GetOutputFileName() {
    return "STDHOSTOUT$";
  }

  static Stdout& GetInstance() {
    static Stdout instance;
    return instance;
  }
};